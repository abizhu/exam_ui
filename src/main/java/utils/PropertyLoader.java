package utils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {
    private static final String PROP_FILE = "login.properties";
    private static final Properties PROPERTIES = new Properties();

    public static String loadProperty(String name) throws IOException {
        File file = new File("login.properties");


        Properties property = new Properties();
        try {
            property.load(PropertyLoader.class.getClassLoader().getResourceAsStream("login.properties"));
        } catch (IOException e) {
            System.err.println("Error: Property file is absent!");
        }
        String value = "";

        if (name != null) {
            value = property.getProperty(name);
        }
        return value;
    }
}

