package Cucumber;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.qameta.allure.Allure;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import pages.WebexLoginPage;
import utils.PropertyLoader;
import pages.WebexHomePage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import pages.WebexLoginPage;
import pages.WebexHomePage;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

public class MyStepdefs {
    private static String USER_INFO = "Persönlicher Raum von Alina Bizhumanova";
    static String BASEURL = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";
    WebexLoginPage page;
    WebexHomePage homePage;

    @After
    public void ontestFail(Scenario scenario) {
        if (scenario.isFailed()) {
            Allure.addAttachment("any text",
                    new ByteArrayInputStream(((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES)));
        }
    }

    @Given("user is logged in")
    public void userIsLoggedIn() throws IOException {
        System.setProperty("selenide.browser", "Chrome");
        open(BASEURL);
        page = open(BASEURL, WebexLoginPage.class);
        page.loginButtonClick();
        page.inputEmailAndConfirm(PropertyLoader.loadProperty("email"));
        page.selectMyPortal();
        page.inputUsername(PropertyLoader.loadProperty("email"));
        page.inputPasswordAndConfirm(PropertyLoader.loadProperty("password"));
        homePage = page.goToHomePage();
    }

    @And("check start meeting button")
    public void checkStartMeetingButton() {
        homePage.checkUserInfo("Persönlicher Raum von Alina Bizhumanova123 Edited");
        homePage.checkStartMeetingButtons();
    }

    @And("click more ways to join")
    public void clickMoreWaysToJoin() {
        homePage.clickMoreWaysToJoin();
    }

    @Then("correct meeting id displayed")
    public void checkMeetingId() {
        homePage.checkMeetingId();
    }
}
