package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.*;

public class WebexLoginPage {

    public void inputUsername(String query) {
        $(By.name("userId")).setValue(query).pressEnter();
//        return page(WebexLoginPage.class);
    }

    public void loginButtonClick() {
        $(By.id("guest_signin_split_button-action")).click();
//        return page(WebexLoginPage.class);
    }

    public void inputEmailAndConfirm(String email) {
        $(By.id("IDToken1")).sendKeys(email);
        $(By.name("btnOK")).click();

//        return page(WebexLoginPage.class);
    }

    public void selectMyPortal() {
        $(By.xpath("//div/span[. = 'MyPortal Account (Alternative)']")).click();
    }


    public void inputPasswordAndConfirm(String query) {
        $(By.name("password")).setValue(query).pressEnter();
    }

    public void clickSubmitButton() {
        $(By.id("submit")).click();
    }

    public WebexHomePage goToHomePage() {
        $(By.id("dashboard_nav_home_item")).click();
        return page(WebexHomePage.class);
    }


    public void getErrorToast(String error) {
        $(By.xpath("//div[@class='info-box error offset-top-3']"))
                .shouldHave(Condition.exactText(error));
    }

    public void checkPageUrl(String expectedUrl) {
        String pageUrl = webdriver().driver().url();
        Assert.assertEquals(pageUrl, expectedUrl, "URL doesn't match!");

    }

}
