package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class WebexHomePage {

    public void checkUserInfo(String string) {
        $(By.cssSelector("div[class^=\"pmr_card_c_title\"]"))
                .shouldHave(Condition.exactText(string));
    }

    public void checkFirstMeeting(String string) {
        $(By.xpath("//div[contains(@class, 'meeting_topic home')]/a"))
                .shouldHave(Condition.ownText(string));
    }

    public void checkStartMeetingButtons() {
        $(byId("smartJoinButton-trigger")).click();
        $$(byXpath("//ul[@id = 'smartJoinButton']/li")).shouldBe(CollectionCondition.texts("Use desktop app", "Use web app"));
    }

    public void clickMoreWaysToJoin() {
        $(byText("More ways to join")).click();
    }
    public void checkMeetingId() {
        $(byAttribute("aria-label", "Copy Meeting Number")).shouldBe(Condition.text("844 839 894"));
    }

}
