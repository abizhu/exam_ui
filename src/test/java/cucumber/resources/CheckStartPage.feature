Feature: Start Page

  Scenario: Check start page
    Given user is logged in
    When check start meeting button
    And click more ways to join
    Then correct meeting id displayed

