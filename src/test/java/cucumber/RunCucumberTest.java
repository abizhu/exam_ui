package cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/cucumber/resources/CheckStartPage.feature"},
        plugin = {"json:target/cucumber-html-reports/cucumber.json", "pretty", "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm"},
        glue = {"classpath:"},
        publish = true
)
public class RunCucumberTest {


}
